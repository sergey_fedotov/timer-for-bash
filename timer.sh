#!/bin/bash
######################### Copyright ##############################################
#
#	"THE BEERWARE LICENSE" (Revision 42):
#	<Servifed2@GMail.com> wrote this file. As long as you retain this notice you
#	can do whatever you want with this stuff. If we meet some day and you think
#	this stuff is worth it, you can buy me a beer in return Sergey Fedotov.
#
##################################################################################
_timer_(){
	[[ -z $1 ]] && echo "false" \
	|| echo "[[ \$(( $[ `date +%s` + $1 ] - \$(date +%s) )) -gt 0 ]]"
}
_timer1_(){
	[[ -z $1 ]] && echo "false" \
	|| echo "\$(( $[ `date +%s` + $1 ] - \$(date +%s) ))" 
}
