#!/bin/bash
######################### Copyright ##############################################
#
#	"THE BEERWARE LICENSE" (Revision 42):
#	<Servifed2@GMail.com> wrote this file. As long as you retain this notice you
#	can do whatever you want with this stuff. If we meet some day and you think
#	this stuff is worth it, you can buy me a beer in return Sergey Fedotov.
#
##################################################################################
#set -x

source timer.sh

if [ -z $1 ]; then

# Create timer for 7 seconds from now
Wait_7sec=`_timer_ 7` \
	&& echo "Timer for 7 seconds created"

# Create timer for 5 seconds from now
Wait_5sec=`_timer_ 5` \
	&& echo "Timer for 5 seconds created"

echo "Wait while Wait_7sec timer is true" 
while eval $Wait_7sec ; do
	#Check status of Wait_5sec timer
	echo "Wait_5sec status: "$( eval $Wait_5sec && echo "True" || echo "False"; )
	sleep 1
done
echo "7 seconds passed"

fi


# Create another timer for 7 seconds from now
Timeout=9
Another_timer=`_timer1_ $Timeout` \
	&& echo "Timer for another $Timeout seconds created"
echo "Wait while Wait_another $Timeout seconds timer is true" 
while `eval [[ $Another_timer -gt 0 ]]` ; do
#	echo -n -e "\r"`eval echo $Another_timer`
	eval echo -n -e "\\\r${Another_timer}"
	sleep 1
done
echo -e "\r"0
echo "another $Timeout seconds passed"
